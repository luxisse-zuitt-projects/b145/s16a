console.log("Hello from JS");

//[SECTION 1] Assignment Operators

// 1. Basic Assignment Operator (=)
// this allows us to add the value of the right operand to a variable and assigns the result to the variable.
let assignmentNumber = 5;

let message = 'This is the message';

// 2. Addition Assignment Operator (+)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
// assignmentNumber = assignmentNumber + 2; //7
console.log("Result of the operation: " + assignmentNumber);

//shorthanded version of the statement above
assignmentNumber += 2;
console.log("Result of the operation: " + assignmentNumber);

	//[SECTION 1: SUB 2] Arithmetic Operators
	// (+, -, *, /)

// 3. Subtraction/Multiplication/Division Assignment Operator (-/*/'/' + =).
assignmentNumber *= 4;
console.log("Result of the operation: " + assignmentNumber);//4

// assignmentNumber = assignmentNumber = 3.

//[RECAP] Assignment Operators

let value = 8;

//addition assignment (+=)
// value += 15;

//subtraction assignment (-=)
// value -= 5; //3

//multiplication assignment (*=)
// value *= 2; //16

//division assignment (/=)
// value /= 2; //4

// console.log(value);

// [SECTION] Arithmetic Operators

// variable = value;

let x = 15;
let y = 10;

// addition (+)
let sum = x + y
console.log(sum);

// subtraction (-)
let difference = x - y;
console.log(difference); //5

// multiplication (*)
let product = x * y;
console.log(product); //150

// division (/)
let quotient = x / y;
console.log(quotient); //1.5

// remainder between the 2 values(Modulus '%')
let remainder = x % y;
console.log(remainder); //5

// [SUBSECTION] Multiple Operators and Parentheses

// when multiple operators are applied in a single statement, it follows the PEMDAS rule. (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

// The operations were done in the following order to get to the final answer.
	// 1. 3 * 4 = 12 (Multiplication)
	// 2. 12 / 5 = 2.4 (Division)
	// 3. 1 + 2 = 3 (Addition)
	// 4. 3 - 2.4 = 0.6 (Subtraction)

// NOTE: The order of operations can be changed by adding parentheses to the logic

let pemdas = 1 + (2 - 3) * (4 / 5); //0.2
console.log(pemdas);

// By adding parentheses '()', the order of the operations are changed prioritizing the operations enclosed within parenthesis.
// This operation was done with the following order:
	// 1. 4 / 5 = 0.8 and 2 - 3 = -1
	// 2. -1 * 0.8 = - 0.8
	// 3. 1 + -0.8 = 0.2

// [SECTION] Increment and Decrement

// Declaration
let z = 1;

// Pre and Post
// Increment (++)

// Pre-increment (syntax: ++variable)

// the value of "z" is added by a value of 1 before returning the value and storing it inside a new variable: "preIncrement".
let preIncrement = ++z; //1+1
console.log(preIncrement); //result of the pre-increment
// we can see here that the value of z was also increased even though we did NOT implicitly specified any variable reassignment.
console.log(z); //2

// Post-increment (syntax: variable++)
let postIncrement = z++;

// the value of z is returned and stored inside the variable called "postIncrement". The value of z is at 2 before it was incremented
console.log(postIncrement); //2
// z + 1
console.log(z);
// 1 + z VS z + 1

// Decrement (--)

// Pre-decrement (syntax: --variableName)
//  the value of Z starts with 3 before it was decremented
let preDecrement = --z;
console.log(preDecrement); //2


// Post-decrement (syntax: variableName--)
let postDecrement = z--;
console.log(postDecrement);

console.log(z);

// 
let bagongValue = 3;

// let newValue = ++bagongValue; //pre increment
// console.log('newValue using pre-increment: ' + newValue); //4

let newValue = bagongValue++;
console.log('newValue using post-increment: ' + newValue);

console.log(newValue); //3
console.log(bagongValue); //4

// real life situations in programming where we use this type of operation?
	// 1. queues
	// 2. creating loop conditions

// [SECTION:] TYPE COERCION

let numberA = 6;
let numberB = '6';

// let's check the data types of the values above
// typeof expression -> will allow us to identify the data type of a certain value/component.
console.log(typeof numberA); //number
console.log(typeof numberB); //string
// coercion = number + string => number data type was converted into a string to perform concatenation instead of addition.
let coercion = numberA + numberB;
console.log(typeof coercion); //concatenation

// Adding number and boolean
let expressionC = 10 + true;
console.log(expressionC); //11 (typeof: number)

let a = true;
console.log(typeof a); //boolean
let b = 10;
console.log(typeof b); //number

// NOTE: The boolean value of "true" is also associated with the value of 1.

let expressionD = true + true + true; //1 + 1 +1; JS reads true = 1; when read with +
console.log(expressionD);

let expressionE = 10 + false; //false === 0
console.log(expressionE);

// In JS, the boolean value of "false" is associated with a value of 0.

let expressionF = true + false; //1
console.log(expressionF);

// Number with a Null value.
let expressionG = 8 + null; //8 (string, number, boolean)
// 8 + 0 = 8
console.log (expressionG);

let d = null;
console.log(typeof d); //object

// Conversion Rules
	// 1. If at least one operand is an object, it will be converted into a primitve value/data type.
	// 2. After conversion, if at least 1 operand is a string data type, the 2nd operand is converted into another string to perform concatenation.
	// 3. In other cases where both operands are converted to numbers, then an arithemetic operation is executed.

// the object data type which is "null" operand was converted into a primitive data type (strings, numbers, and booleans - can only contain a single value).

// String with a null data type
let expressionH = "Batch145" + null; //null will be converted into a primitive data type. null -> "null"
// "Batch145" + "null" = "Batch145null"
console.log(expressionH); //Batch145null

// 1. Batch 145 -> string data type
// 2. null -> object data type

// Number with undefined data type
expressionH = 9 + undefined; //NaN -> Not a Number
console.log(expressionH);

let e = undefined;
console.log(typeof e);

// 1. undefined was converted into a number data type NaN
// 2. 9 + NaN = NaN

let expressionJ = true + true + 'hello' + true + true;
console.log(typeof expressionJ);

// [SECTION] Comparison Operators
let name = 'Juan';

	// [SUB SECTION] EQUALITY OPERATORS (==)
	// -> attempts to CONVERT and COMPARE operands with 2 different data types.
	// -> returns a boolean value
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(1 == true); //true
	console.log(1 == false); //false
	console.log(name == 'Juan'); //true
	console.log(name == 'John'); //false
	console.log('Juan' == 'juan'); //false; JS is case-sensitive
	// console.log('Juan' == Juan); //Uncaught ReferenceError: Juan is not defined //This is an error because the variable was not yet declared.

	// [SUB SECTION] Inequality Operator (!=)

	// -> checks whether the operands are NOT EQUAL/HAVE DIFFERENT VALUES.

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false (different data types)
	console.log(0 != false); //false (false is associated with 0 in JS)
	let juan = 'juan';
	console.log('juan' != juan); //false (because it's the same value)

	// [SUB SECTION] "STRICT" EQUALITY OPERATOR (===)
	// -> checks whether the operands are equal or have the same value.
	// -> Also compares if the data types are the same.
	
	console.log(1 === 1); //true
	console.log(1 === '1'); //false (number != string)
	console.log(0 === false); //false (number != boolean)
	// They have different data types hence, false.

	// [SUB SECTION] "STRICT" INEQUALITY OPERATOR (!==)

	// -> this will check if the operands are NOT EQUAL/ HAVE DIFFERENT values/content.
	// -> checks both values and data types of both components/operands.
	// will not attempt to convert data types

	console.log(1 !== 1); //false (same values & data types)
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(0 !== false); //true
	console.log(1 !== true); //true

// Developer's tip: upon creating conditions or statements, it is strongly recommended to use "strict" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario.

// [SECTION] Relational Operators
let priceA = 1800;
let priceB = 1450;

// lesser than operator
console.log(priceA < priceB); //false
// greater than operator
console.log(priceA > priceB); //true

let expressionI = 150 <= 150;
console.log(expressionI);

// Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. It is a writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.

// is + Single = true;
// are + Taken = false;
// are + Listening = true;

isLegalAge = true;
isRegistered = false;

// for the person to be able to vote, both requirements have to be met.

// we need to use the Proper Logical operator
// AND (&& Double Ampersand) all criteria has to be MET.
let isAllowedToVote = isLegalAge && isRegistered;
console.log('Is the person allowed to vote? ' + isAllowedToVote);

// OR (|| Double Pipe) - evaluates whether some are true; at least one criteria has to be MET in order to pass

let isAllowedForVaccination = isLegalAge || isRegistered;
console.log('Is the person allowed for vaccination? ' + isAllowedForVaccination); //true

// NOT OPERATOR (! - Exclamation Point)
	// This will convert/return the opposite value of a boolean.

	let isTaken = true;
	let isTalented = false;

	console.log(!isTaken);
	console.log(!isTalented); //true