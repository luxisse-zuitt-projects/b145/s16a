console.log("Hello World");

let x = 8;
let y = 16;

// sum
let sum = x + y
console.log('The sum of the two numbers is: ' + sum); //24

// difference
let difference = x - y;
console.log('The difference of the two numbers is: ' + difference); //-8

// product
let product = x * y;
console.log('The product of the two numbers is: ' + product); //128

// quotient
let quotient = x / y;
console.log('The quotient of the two numbers is: ' + quotient); //0.5

//check if the sum is greater than the difference
let isGreater = sum > difference;
console.log('The sum is greater than the difference: ' + isGreater); //true


// Using comparison and logical operators print out a string in the console that will return a message if the product and quotient are both positive numbers.

let arePositiveNumbers = product > 0 && quotient > 0;
console.log('The product and quotient are positive numbers: ' + arePositiveNumbers);


// Using comparison and logical operators print out a string in the console that will return a message if one of the results is a negative number.

let isAResultNegative = sum < 0 || difference < 0 || product < 0 || quotient < 0;

console.log('One of the results is negative: ' + isAResultNegative); //true


// Using comparison and logical operators print out a string in the console that will return a message if one of the results is zero.

let isAResultZero = sum != 0 || difference != 0 || product != 0 || quotient != 0;

console.log('All the results are not equal to zero: ' + isAResultZero); //true